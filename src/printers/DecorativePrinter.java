package printers;

public class DecorativePrinter implements IPrinter {
    @Override
    public void print(String text) {
        System.out.println(" * ");
        System.out.println(text);
        System.out.println(" * ");
    }
}
