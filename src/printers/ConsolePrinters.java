package printers;

public class ConsolePrinters implements IPrinter {
    @Override
    public void print(String text) {
        System.out.print(text);
    }
}
