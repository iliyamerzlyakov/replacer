import printers.AdvConcolePrinter;
import printers.ConsolePrinters;
import printers.DecorativePrinter;
import printers.IPrinter;
import readers.IReader;
import readers.PredefinedReader;

public class Main {
    public static void main(String[] args) {
        IReader reader = new PredefinedReader("Привет:) Пока-пока:)");
        IPrinter printer = new ConsolePrinters();
        IPrinter advPrinter = new AdvConcolePrinter();
        IPrinter decoPrinter = new DecorativePrinter();
        Replacer replacer = new Replacer(reader, printer);
        Replacer advReplacer = new Replacer(reader, advPrinter);
        Replacer decoReplacer = new Replacer(reader, decoPrinter);
        replacer.replace();
        advReplacer.replace();
        decoReplacer.replace();
    }
}
