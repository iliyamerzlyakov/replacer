package readers;

public class PredefinedReader implements IReader {
    private String text;

    public PredefinedReader(String text) {
        this.text = text;
    }

    @Override
    public String read() {
        return text;
    }
}

